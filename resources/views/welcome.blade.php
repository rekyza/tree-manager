<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tree Manager</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light justify-content-center">
    <a class="navbar-brand" href="{{ url('/') }}">Tree Manager by rekyza</a>
        <div class="navbar-nav">
            <a class="btn btn-success" href="{{ url('/') }}">Show tree<span class="sr-only">(current)</span></a>
            <a class="btn btn-dark" href="{{ url('/trees') }}">Manage vertex's</a>
            <a class="btn btn-primary" href="{{ url('/trees/create') }}">Add vertex</a>
            <a class="btn btn-info" href="{{ url('/trees/connect') }}">Add edge</a>
        </div>
</nav>

<div id="welcome" class="container">
    @yield('main')
</div>
<script src="{{ asset('js/app.js') }}" type="text/js"></script>
</body>
</html>
