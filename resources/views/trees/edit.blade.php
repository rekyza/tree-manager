@extends('welcome')
@section('main')
    <div class="row">
        <div class="col-sm-4"><h1 class="display-3">Update element in the Tree</h1></div>
        <div class="col-sm-8">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <br />
            @endif
            <form method="post" action="{{ route('trees.update', $tree->id) }}">
                @method('PATCH')
                @csrf
                <div class="form-group">
                    <label for="first_name">Name:</label>
                    <input type="text" class="form-control" name="name" value={{ $tree->name }} />
                </div>

                <div class="form-group">
                    <label for="last_name">Position:</label>
                    <input type="text" class="form-control" name="position" value={{ $tree->position }} />
                </div>

                <div class="form-group">
                    <label for="email">Level:</label>
                    <input type="text" class="form-control" name="level" value={{ $tree->level }} />
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>
@endsection
