@extends('welcome')

@section('main')
    <div class="row">
        <div class="col-sm-6">
            <h1 class="display-3">Vertex manager</h1>
        </div>
        <div class="col-sm-6 justify-content-end">
            @if(session()->get('info'))
                <div class="alert alert-info">
                    {{ session()->get('info') }}
                </div>
            @endif
        </div>
        <div class="col-md-10">
            <table class="table table-striped">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>Name</td>
                    <td>Position</td>
                    <td>Level</td>
                    <td colspan = 2>Actions</td>
                </tr>
                </thead>
                <tbody>
                @foreach($trees as $tree)
                    <tr>
                        <td>{{$tree->id}}</td>
                        <td>{{$tree->name}}</td>
                        <td>{{$tree->position}}</td>
                        <td>{{$tree->level}}</td>
                        <td>
                            <a href="{{ route('trees.edit',$tree->id)}}" class="btn btn-primary">Edit</a>
                        </td>
                        <td>
                            <form action="{{ route('trees.destroy', $tree->id)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-2">
            <form method="get" action="/trees/sortByPosition">
                <button class="btn btn-info" type="submit">Sort by Position</button>
            </form>
            <form method="get" action="/trees/sortByLevel">
                <button class="btn btn-info" type="submit">Sort by Level</button>
            </form>
        </div>
    </div>
@endsection
