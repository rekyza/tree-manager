@extends('welcome')

@section('main')
    <div class="row">
        <div class="col-sm-12 justify-content-center align-content-center">
            <canvas></canvas>
        </div>
    </div>
    <script type="text/javascript">
        const arrayTree = {!! $trees !!};
        //console.log(arrayTree[0]['id']);
        //console.log(Object.keys(arrayTree).length);

        const canvas = document.querySelector('canvas');
        fitToContainer(canvas);
        const ctx = canvas.getContext('2d');
        const wCanvas = canvas.width;
        const hCanvas = canvas.height;
        ctx.clearRect(0, 0, wCanvas, hCanvas);

        function fitToContainer(canvas){
            // Make it visually fill the positioned parent
            canvas.style.width ='100%';
            // ...then set the internal size to match
            canvas.width  = canvas.offsetWidth;
            canvas.height = document.documentElement.clientHeight-100;
        }

        function ball(id, x, y, text){
            this.x = x;
            this.y = y;
            this.text = text;
            this.radius = 35;
            this.color = '#fff';
            this.id = id;
            this.draw = function(){
                ctx.beginPath();
                ctx.arc(this.x, this.y, this.radius, 0, 2*Math.PI, false);
                ctx.fillStyle = '#007bff';
                ctx.fill();
                ctx.closePath();
                ctx.beginPath();
                ctx.fillStyle = '#343a40';
                ctx.fillText(this.text, this.x-25, this.y-30);
                ctx.fill();
                ctx.closePath();
            }
        }

        function line(xs, ys, xf, yf){
            this.xs = xs;
            this.ys = ys;
            this.xf = xf;
            this.yf = yf;
            this.draw = function(){
                ctx.beginPath();
                ctx.moveTo(xs,ys);
                ctx.lineTo(xf,yf);
                ctx.strokeStyle="green";
                ctx.stroke();
            }
        }
        const edgesTree = {!! $edges !!};
        const balls = [];

        function renderBalls(){
            let x = 0;
            let y = 0;
            let text = '';
            let id = 0;
            for(let i = 0; i < Object.keys(arrayTree).length; i++) {
                    y = 50 + arrayTree[i]['level']*150;
                    x = 50 + arrayTree[i]['position']*150;
                    text = arrayTree[i]['name'];
                    id = arrayTree[i]['id'];
                    console.log(text);
                    balls[i] = new ball(id, x, y, text);
                    balls[i].draw();
            }
        }

        function renderLine(){
            const lineTree = [];
            for(let ed = 0; ed < Object.keys(edgesTree).length; ed++) {
                lineTree[ed] = [];
                for(let ba = 0; ba < Object.keys(balls).length; ba++) {
                    if (edgesTree[ed]['id_vertex_child'] == balls[ba]['id']) {
                        lineTree[ed]['xs'] = balls[ba]['x'];
                        lineTree[ed]['ys'] = balls[ba]['y'];
                    }
                    if (edgesTree[ed]['id_vertex_parent'] == balls[ba]['id']) {
                        lineTree[ed]['xf'] = balls[ba]['x'];
                        lineTree[ed]['yf'] = balls[ba]['y'];
                    }
                }
                line[ed] = new line(lineTree[ed]['xs'],lineTree[ed]['ys'],lineTree[ed]['xf'],lineTree[ed]['yf']);
                line[ed].draw();
            }
        }
        function welcomeText(text){
            ctx.font = 'normal bold 60px sans-serif';
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            ctx.fillText(text, canvas.width/2, canvas.height/4);
        }

        document.addEventListener("click", function() {
            ctx.font = 'normal bold 14px sans-serif';
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            renderBalls();
            renderLine();
        });
        welcomeText("Click to run view");
        console.log(balls);

    </script>
@endsection
