@extends('welcome')

@section('main')
    <div class="row">
        <div class="col-sm-4"><h1 class="display-3">Connect the Vertex by Edge</h1></div>
        <div class="col-sm-8">
            <div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                @endif
                <form method="post" action="/trees/connect">
                    @csrf
                    <div class="form-group">
                        <label for="forParent">Vertex:</label>
                        <select id="forParent" class="form-control" name="forParent">
                            <option value="" selected></option>
                            @foreach($vertices as $vertex)
                                <option value="{{$vertex->id}}">{{$vertex->name}}</option>
                            @endforeach
                        </select>
                        <label for="parentVertex">Parent of Vertex:</label>
                        <select id="parentVertex" class="form-control" name="parentVertex">
                            <option value="" selected></option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-success">Add edge</button>
                </form>
            </div>
        </div>
    </div>
    <script>
        class addEdge{
            static getValue() {
                const arrayTree = {!! $vertices !!};
                console.log(arrayTree);
                //console.log("Otwieramy nawigacje");
                let strVertex = selectForm.selectedIndex-1;
                console.log(strVertex);
                let levelParentVertex;
                levelParentVertex = arrayTree[strVertex]['level']-1;
                console.log(levelParentVertex);
                if(strVertex>=0)
                {
                    console.log(levelParentVertex);
                    const parrentOfVertexTree = new Array();
                    for(let i = 0; i < Object.keys(arrayTree).length; i++) {
                        if(arrayTree[i]['level']==levelParentVertex)
                        {
                            parrentOfVertexTree.push(arrayTree[i]);
                        }
                    }
                    //console.log(Object.keys(parrentOfVertexTree));
                    let select = document.getElementById("parentVertex");
                    document.getElementById("parentVertex").innerHTML = ' ';
                    for(let i = 0; i < Object.keys(parrentOfVertexTree).length; i++) {
                        //console.log(parrentOfVertexTree[i]);
                        //console.log(parrentOfVertexTree[i]['name']);
                        select.options[select.options.length] = new Option(parrentOfVertexTree[i]['name'], parrentOfVertexTree[i]['id']);
                    }
                    //return parrentOfVertexTree;
                    //alert(strVertex);
                }
            }
        }
        const selectForm = document.getElementById( 'forParent' );
        selectForm.onclick = addEdge.getValue;
    </script>
@endsection
