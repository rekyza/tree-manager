@extends('welcome')

@section('main')
    <div class="row">
        <div class="col-sm-4"><h1 class="display-3">Add element to the Tree</h1></div>
        <div class="col-sm-8">
            <div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                @endif
                <form method="post" action="{{ route('trees.store') }}">
                    @csrf
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" name="name"/>
                    </div>

                    <div class="form-group">
                        <label for="level">Level:</label>
                        <input type="number" class="form-control" name="level"/>
                    </div>

                    <div class="form-group">
                        <label for="position">Position:</label>
                        <input type="number" class="form-control" name="position"/>
                    </div>
                    <button type="submit" class="btn btn-success">Add vertex</button>
                </form>
            </div>
        </div>
    </div>
@endsection
