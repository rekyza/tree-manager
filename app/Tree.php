<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tree extends Model
{
    protected $fillable = [
        'name',
        'position',
        'level'
    ];

    public static function allPositionDescending()
    {
        $tree = Tree::all();
        return $tree->sortBy('position');
    }

    public static function allLevelDescending()
    {
        $tree = Tree::all();
        return $tree->sortBy('level');
    }

    public static function addWithChangePositionOld($request) {
        $requestPosition = $request->input('position');
        $requestLevel = $request->input('level');
        $requestName = $request->input('name');
        $allVertex = Tree::all();
        for($i=0;$i<count($allVertex);$i++){
            if($allVertex[$i]->name == $requestName){
                $requestName = $requestName.' new';
            }
            if($allVertex[$i]->position == $requestPosition && $allVertex[$i]->level == $requestLevel){
                for($j=0;$j<count($allVertex);$j++) {
                    $oldVertexPosition = $allVertex[$j]->position + 1;
                    $oldVertexLevel = $allVertex[$j]->level;
                    $oldVertexId = $allVertex[$j]->id;
                    $oldVertexName = $allVertex[$j]->name;
                    $updateVertex = Tree::find($oldVertexId);
                    $updateVertex->name = $oldVertexName;
                    $updateVertex->position = $oldVertexPosition;
                    $updateVertex->level = $oldVertexLevel;
                    $updateVertex->save();
                    $requestPosition++;
                }
            }
        }
        $add = new Tree([
            'name' => $requestName,
            'position' => $requestPosition,
            'level' => $requestLevel
        ]);
        $add->save();
        $result="Add vertex :)";
        return $result;
    }

}
