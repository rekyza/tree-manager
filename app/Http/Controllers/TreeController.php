<?php

namespace App\Http\Controllers;

use App\Edges;
use Illuminate\Http\Request;
use App\Tree;

class TreeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edge(Request $request)
    {
        $parentVertex = $request->parentVertex;
        $forParent = $request->get('forParent');
        if (isset($parentVertex) && isset($forParent)) {
            $edges = new Edges([
                'id_vertex_child' => $parentVertex,
                'id_vertex_parent' => $forParent
            ]);
            $edges->save();
        }
        else {
            return redirect('/trees')->with('info', 'Check values new edge.');
        }

        return redirect('/trees')->with('info', 'Edges saved!');
    }

    public function connect(){
        $vertices = Tree::all();
        return view('trees.connect', compact('vertices'));
    }

    public function jsTree()
    {
        $trees = Tree::all();
        $edges = Edges::all();
        return view('trees.tree', compact('trees','edges'));
    }

    public function index()
    {
        $trees = Tree::allPositionDescending();

        return view('trees.index', compact('trees'));
    }

    public function sortByLevel()
    {
        $trees = Tree::allLevelDescending();

        return view('trees.index', compact('trees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('trees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'position'=>'required',
            'level'=>'required'
        ]);
        $result=Tree::addWithChangePositionOld($request);
        return redirect('/trees')->with('info', $result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tree = Tree::find($id);
        return view('trees.edit', compact('tree'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            'position'=>'required',
            'level'=>'required'
        ]);

        $tree = Tree::find($id);
        $tree -> delete();
        $result=Tree::addWithChangePositionOld($request);
        return redirect('/trees')->with('info', $result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = Tree::find($id);
        $contact -> delete();

        return redirect('/trees')->with('info', 'Tree deleted!');
    }
}
