<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Edges extends Model
{
    protected $fillable = [
        'id_vertex_child',
        'id_vertex_parent'
    ];

}
