# How to navigate the project?
## used technologies

 - Laravel
	 - app
		 - Controller: TreeController
		 - Models: Tree & Edges
	- database
		- migrations
	- public
		- css: app.css (contains bootstrap)
	- resources
		- views: welcome (main content), trees (attached views)
	- routes
		- web.php: routing controller with views
- Javascript
	- was injected into individual views
- MySQL
	- laravel config/database.php

# Screenshots
Start app
![start](readme/index.png)
Run show tree
![show tree](readme/tree.png)
Manage vertex's
![manager](readme/managevertex.png)
Add edge
![add edge](readme/edge.png)
Add vertex
![add vertex](readme/addvertex.png)
